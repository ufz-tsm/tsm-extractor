FROM debian:bullseye-slim as base

ARG BUILD_DATE
ARG VCS_REF

LABEL maintainer="The ZID@UFZ Team" \
    org.opencontainers.image.title="Extractor Base Image" \
    org.opencontainers.image.licenses="EuGPL 1.2" \
    org.opencontainers.image.version="0.3.0" \
    org.opencontainers.image.revision=$VCS_REF \
    org.opencontainers.image.created=$BUILD_DATE

# dependencies:
# - python-tk       -> SaQC
# - ca-certificates -> postgres
# - libpq-dev       -> psycopg
# - git             -> pip
RUN apt-get -y update \
    && apt-get -y dist-upgrade \
    && apt-get -y --no-install-recommends install \
      python3 \
      python3-pip \
      python3-tk \
      git \
      ca-certificates \
      libpq-dev \
    && apt-get -y autoremove \
    && apt-get -y autoclean

# Create a group and user
RUN useradd --uid 1000 -m appuser

# COPY --chown=appuser --from=build /root/.local /home/appuser/.local

# Tell docker that all future commands should run as the appuser user
USER appuser

# add requirements
COPY requirements.txt requirements.txt

RUN pip install --upgrade pip \
    && pip install \
    --user \
    --no-cache-dir \
    --no-warn-script-location -r \
    requirements.txt

WORKDIR /home/appuser/app

COPY extractor extractor

WORKDIR /home/appuser/app/extractor

# singularity needs full paths
ENTRYPOINT ["python3", "/home/appuser/app/extractor/main.py"]
