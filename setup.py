#! /usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

__version__ = "0.4.0"

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="tsm_extractor",
    version=__version__,
    author="The ZID Team at UFZ",
    description="Library to parse raw data from various sources",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://codebase.helmholtz.cloud/ufz-tsm/tsm-extractor/",
    license="EUPL1.2",
    packages=find_packages(),
    python_requires=">=3.9",
    install_requires=[
        "pandas>=2.0",
        "psycopg[binary]",
        "requests",
        "saqc==2.6.0",
        "tsm-user-code @ git+https://codebase.helmholtz.cloud/ufz-tsm/tsm-dataprocessing-extension.git",
    ],
)
