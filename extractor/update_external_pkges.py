#!/usr/bin/env python3
from __future__ import annotations

import subprocess
import sys


def update_tsm_user_code():
    subprocess.check_call(
        [
            sys.executable,
            "-m",
            "pip",
            "install",
            "git+https://codebase.helmholtz.cloud/ufz-tsm/tsm-dataprocessing-extension.git",
        ]
    )


if __name__ == "__main__":
    update_tsm_user_code()
