#!/usr/bin/env python
from __future__ import annotations
import os

import json
import logging
from typing import Any, Literal

import psycopg
import requests
import pandas as pd

from extractor.parser import AbstractParser, CsvParser

logger = logging.getLogger("data-parser")
logger.addHandler(logging.NullHandler())

try:
    import tsm_user_code  # noqa, this registers user functions on SaQC
except ImportError:
    logger.warning("could not import module 'tsm_user_code'")


def load_parser_settings(thing_id) -> tuple[str, dict[str, Any]]:

    default_settings = {
        "comment": "#",
        "decimal": ".",
        "na_values": None,
        "encoding": "utf-8",
        "engine": "python",
        "on_bad_lines": "warn",
        "header": None,
    }

    dsn = os.environ.get(
        "CONFIGDB_DSN", "postgresql://configdb:configdb@localhost:5432/postgres"
    )

    with psycopg.connect(dsn) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT file_parser_type.name, file_parser.params FROM thing
                JOIN s3_store ON thing.s3_store_id = s3_store.id
                JOIN file_parser ON s3_store.file_parser_id = file_parser.id
                JOIN file_parser_type ON file_parser.file_parser_type_id = file_parser_type.id
                WHERE thing.uuid = %s;
                """,
                (thing_id,),
            )
            result = cur.fetchone()

    parser_type, settings = result
    kwargs = settings.pop("pandas_read_csv") or {}
    settings = {**default_settings, **kwargs, **settings}
    settings["index_col"] = settings.pop("timestamp_column")
    settings["date_format"] = settings.pop("timestamp_format")

    return parser_type, settings


def load_parser(thing_id) -> AbstractParser:
    parser_type, settings = load_parser_settings(thing_id)
    if parser_type == "csvparser":
        return CsvParser(settings)
    raise ValueError(f"unknown parser typer '{parser_type}")


def write_observations(thing_id: str, source_uri: str, df: pd.DataFrame):
    def _convert(df):
        observations = []

        # NOTE: this is actually the wrong field, we should use phenomenon time instead
        # prep index
        df.index.name = "result_time"
        df.index = df.index.strftime("%Y-%m-%dT%H:%M:%S%Z")

        for col in df.columns:
            chunk = df[col]
            if pd.api.types.is_numeric_dtype(chunk):
                chunk.name = "result_number"
                result_type = 0
            elif pd.api.types.is_string_dtype(chunk):
                chunk.name = "result_string"
                result_type = 1
            elif pd.api.types.is_bool_dtype(chunk):
                chunk.name = "result_bool"
                result_type = 2
            else:
                raise NotImplementedError(
                    f"data of type {chunk.dtype} is not supported"
                )

            # we don't want to write NaN
            chunk = chunk.dropna()
            # add metadata
            chunk = chunk.reset_index()
            chunk["result_type"] = result_type
            chunk["datastream_pos"] = str(col)
            chunk["parameters"] = json.dumps(
                {"origin": source_uri, "column_header": col}
            )

            observations.extend(chunk.to_dict(orient="records"))
        return {"observations": observations}

    payload = _convert(df)
    host = os.environ.get("DB_API_BASE_URL", "http://localhost:8001")

    response = requests.post(f"{host}/observations/upsert/{thing_id}", json=payload)
    if response.status_code not in (200, 201):
        raise RuntimeError(
            f"upload to {thing_id} failed with {response.reason} and {response.text}"
        )


def parse(
    thing_id: str,
    rawdata: str,
    source_uri: str,
) -> None:
    """Parse the given raw data into the time.IO timeseries database"""

    err = ""
    try:
        err = f"failed to load parser for {thing_id}"
        parser = load_parser(thing_id)
        err = f"failed to parse data with parser={parser!r}"
        data = parser.do_parse(rawdata)
        err = f"failed to write observations for {thing_id} from {source_uri}"
        write_observations(thing_id, source_uri, data)
    except Exception:
        logger.error(err)
        raise
    logger.info("successfully parsed data")
