#! /usr/bin/env python
# -*- coding: utf-8 -*-
from io import StringIO

import pandas as pd

from .abstract_parser import AbstractParser

"""
A basic CSV-Parser.

The actual file reading and most `parser_parameters` are
directly redirected to the external function `pandas.read_csv`.
All currently supported parser arguments are listed in the module
global variables `REQUIRED_SETTINGS` and `DEFAULT_SETTINGS`.
For a detailed description of all available parameters
and their semantics, please refer to the pandas documentation:
https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html
"""


class CsvParser(AbstractParser):
    def do_parse(self, rawdata: str) -> pd.DataFrame:
        """
        rawdata: the unparsed content
        NOTE: we need to prserve the original column numbering. check for the date index column
        """
        index_col = self.settings.pop("index_col")
        try:
            df = pd.read_csv(StringIO(rawdata), **self.settings)
        except (pd.errors.EmptyDataError, IndexError):
            df = pd.DataFrame()
        # remove all nan-columns as artifacts
        df = df.dropna(axis=1, how="all")
        # column positions as header
        df.columns = range(df.shape[1])
        # convert to datetime index
        df.index = pd.to_datetime(
            df.pop(index_col).str.strip(), format=self.settings["date_format"]
        )
        # we don't want an index name
        df.index.name = None
        self.logger.debug(f"data.shape={df.shape}")
        return df
