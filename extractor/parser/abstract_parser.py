import logging
from abc import abstractmethod, ABC

from typing import Any

import pandas as pd


class AbstractParser(ABC):
    def __init__(self, settings: dict[str, Any]):
        self.logger = logging.getLogger(self.__class__.__qualname__)
        self.name = self.__class__.__name__
        self.settings = settings
        self.logger.debug(f"parser settings in use with {self.name}: {self.settings}")

    @abstractmethod
    def do_parse(self, rawdata: str) -> pd.DataFrame:
        raise NotImplementedError
