click==8.1.7
psycopg[binary]==3.2.1
requests==2.32.3
saqc==2.6.0
# requirements for the SaQC user-code
git+https://codebase.helmholtz.cloud/ufz-tsm/tsm-dataprocessing-extension.git
